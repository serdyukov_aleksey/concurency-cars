package workers;

import io.restassured.RestAssured;
import org.apache.log4j.Logger;
import pojo.Car;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CarPainter {
    private static CarPainter instance;
    private final Logger log = Logger.getLogger(getClass());
    private final static String host = "http://localhost:8888/car-details/";
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public static CarPainter getInstance() {
        if (instance == null) {
            instance = new CarPainter();
        }
        return instance;
    }

    public void start (){
        executorService.execute(new PaintCar());
    }

    class PaintCar implements Runnable {
        @Override
        public void run() {
            CarProducer carProducer = CarProducer.getInstance();
            while (true) {
                Car car = carProducer.getBlankCar().get();
                car.setColor(RestAssured.get(host + "color").getBody().as(String.class));
                carProducer.addColoredCar(car);
            }
        }
    }
}