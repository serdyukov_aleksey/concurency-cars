package workers;

import io.restassured.RestAssured;
import org.apache.log4j.Logger;
import pojo.Car;

import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;


public class CarProducer {
    private static CarProducer instance;
    private BlockingQueue<Car> blankCarQueue = new LinkedBlockingQueue<>();
    private BlockingQueue<Car> coloredCarQueue = new LinkedBlockingQueue<>();
    private final Logger log = Logger.getLogger(getClass());
    private final static String host = "http://localhost:8888/car-details/";
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    public static CarProducer getInstance() {
        if (instance == null) {
            instance = new CarProducer();
        }
        return instance;
    }

    public void start (){
        executorService.execute(new CarProducer.CreateNewBlankCar());
        executorService.execute(new CarProducer.PushColoredCarToStock());
    }

    public void addColoredCar(Car car) {
        try {
            coloredCarQueue.put(car);
        } catch (InterruptedException e) {
            log.error("Error adding blank car to blankCarQueue: " + e.getMessage());
        }
    }

    public Optional<Car> getBlankCar (){
        Optional <Car> car = null;
        try {
            car = Optional.of(blankCarQueue.take());
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        }
        return car;
    }


    class CreateNewBlankCar implements Runnable {
        @Override
        public void run() {

            while (true) {
                Car car = new Car();
                try {
                    car.setWheelRadius(RestAssured.get(host + "wheel-radius").getBody().as(int.class));
                    car.setEngine(RestAssured.get(host + "engine").getBody().as(float.class));
                    car.setBody(RestAssured.get(host + "body").getBody().as(String.class));
                    blankCarQueue.put(car);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
            }
        }
    }

    class PushColoredCarToStock implements Runnable {
        @Override
        public void run() {
            Car car;
            while (true) {
                try {
                    car = coloredCarQueue.take();
                    CarMarketPusher.getInstance().addCar(car);

                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
            }
        }
    }
}
