package workers;

import io.restassured.RestAssured;
import org.apache.log4j.Logger;
import pojo.Car;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

public class CarMarketPusher {
    private static CarMarketPusher instance;
    private BlockingQueue<Car> carsStockQueue = new LinkedBlockingQueue<>();
    private final Logger log = Logger.getLogger(getClass());
    private final ExecutorService executorService = Executors.newFixedThreadPool(2);
    private final static String host = "http://localhost:8888/car-details/";

    public static CarMarketPusher getInstance() {
        if (instance == null) {
            instance = new CarMarketPusher();
        }
        return instance;
    }
    public void start (){
        executorService.execute(new PushToMarket());
    }

    public void addCar(Car car) {
        try {
            carsStockQueue.put(car);
        } catch (InterruptedException e) {
            log.error("Error adding message to MessageQueue: " + e.getMessage());
        }
    }

    class PushToMarket implements Runnable {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Car car = carsStockQueue.take();
                    car.setPrice(RestAssured.get(host + "price").getBody().as(int.class));
                    System.out.println("Car #"+car.getCarId()+" : "+car);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
            }
        }
    }
}

