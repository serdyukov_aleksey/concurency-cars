import org.apache.log4j.Logger;
import workers.CarMarketPusher;
import workers.CarPainter;
import workers.CarProducer;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        String jarExec = new File("car-details-server/car-details.jar").getAbsolutePath();
        try {
            Desktop.getDesktop().open(new File(jarExec));  //Runtime.getRuntime().exec - not working
        } catch (IOException e) {
            Logger.getLogger(Main.class).error(e.getMessage());
        }
        CarProducer.getInstance().start();
        CarPainter.getInstance().start();
        CarMarketPusher.getInstance().start();
    }
}
