package pojo;

import java.util.concurrent.atomic.AtomicInteger;

public class Car {
    private int wheelRadius;
    private float engine;
    private String body;
    private String color;
    private int price;
    private int carId;
    private static final AtomicInteger count = new AtomicInteger(0);

    public Car (){
        this.carId = count.incrementAndGet();
    }

    public void setWheelRadius(int wheelRadius) {
        this.wheelRadius = wheelRadius;
    }

    public void setEngine(float engine) {
        this.engine = engine;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCarId() {
        return carId;
    }

    @Override
    public String toString() {
        return "Car{" +
                " wheelRadius=" + wheelRadius +
                ", engine=" + engine +
                ", body='" + body + '\'' +
                ", color='" + color + '\'' +
                ", price=" + price +
                '}';
    }
}

